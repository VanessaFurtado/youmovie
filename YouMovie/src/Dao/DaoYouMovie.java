package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import Modelo.Filme;
import Modelo.Cinema;
import Modelo.Compra;
import Modelo.Usuario;
import Visao.TelaInicialGUI;

public class DaoYouMovie {

	 private final String URL = "jdbc:mysql://localhost/youmovie",
             USER = "root", 
             PASSWORD = "1234";

	private Connection con;
	private Statement comando;
	
	private void conectar() {
	try {
	con = ConFactory.conexao(URL, USER, PASSWORD, ConFactory.MYSQL);
	comando = con.createStatement();
	System.out.println("Conectado!");
	} catch (ClassNotFoundException e) {
	imprimeErro("Erro ao carregar o driver", e.getMessage());
	} catch (SQLException e) {
	imprimeErro("Erro ao conectar", e.getMessage());
	}
	}
	
	private void fechar() {	
		try {
			comando.close();
			con.close();
			System.out.println("Conex‹o Fechada");
		} catch (SQLException e) {
			imprimeErro("Erro ao fechar conex‹o", e.getMessage());
		}
	}
	
	private void imprimeErro(String msg, String msgErro) {
		JOptionPane.showMessageDialog(null, msg, "Erro cr’tico", 0);
		System.err.println(msg);
		System.out.println(msgErro);
		System.exit(0);
	}
	
	public ArrayList<Usuario> consulta() throws Exception{
		conectar();
		
		ArrayList<Usuario> listaUsuario = new ArrayList<Usuario>();
		
		Statement st;
		st = con.createStatement();
		
		ResultSet rs = st.executeQuery("SELECT nome, senha FROM cadastro");
		
		while(rs.next()){
			
			Usuario u = new Usuario();
			u.setNome(rs.getString("nome"));
			u.setSenha(rs.getString("senha"));
			
			listaUsuario.add(u);			
		}
		fechar();
		System.out.println("Consulta bem sucedida");
		
		return listaUsuario;
	}
	
	public ArrayList <Filme> consultaPorNome(String nome) throws SQLException{
		conectar();
		
		ArrayList<Filme> consultaFilme = new ArrayList<Filme>();

		Statement st;
		st = con.createStatement();
		
		ResultSet rs = st.executeQuery("SELECT * FROM filme WHERE nome='"+ nome + "'");

		while(rs.next()){
			
			Filme f = new Filme();
			f.setSinopse(rs.getString("sinopse"));
			f.setClassificacao(rs.getString("classificacao"));
			f.setEtaria(rs.getInt("etaria"));
			f.setPreco(rs.getFloat("preco"));
			f.setFoto(rs.getString("imagem"));
		
			consultaFilme.add(f);
		}
		
		fechar();
		System.out.println("Consulta bem sucedida");
		
		return consultaFilme;
	}
	
	public String consultaPorID(String usuario) {
		conectar();

		String nome="";
		Statement st;
		try{
		st = con.createStatement();
		
		ResultSet rs = st.executeQuery("SELECT nome FROM cadastro WHERE usuario='"+ usuario + "'");

		while(rs.next()){
			
			nome= rs.getString("nome");
			
		}}
		catch(SQLException e){}
			
		fechar();
		System.out.println("Consulta bem sucedida");
		
		return nome;
	}
	
	public ArrayList<Cinema> consultaCinema(String nome) throws Exception{
		conectar();
		ArrayList<Cinema> consultaCinema = new ArrayList<Cinema>();
		
		Statement st;
		st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM cinema WHERE Nome='"+ nome + "'");

		while(rs.next()){
			Cinema c = new Cinema();
			c.setEndereco(rs.getString("endereco"));
			c.setCEP(rs.getInt("CEP"));
			c.setImagem(rs.getString("imagem"));
			consultaCinema.add(c);
			
		}
		fechar();
		System.out.println("Consulta bem sucedida");
		
		return consultaCinema;
	}

	public ArrayList <Filme> listaTodosFilmes(){
		conectar();
		ArrayList <Filme> filme= new ArrayList<Filme>();
		
		try
			{
			PreparedStatement selecionaFilme = null;
			ResultSet rs = null;
			
			String sql = "SELECT * FROM FILME";
			
			selecionaFilme = con.prepareStatement(sql);
			rs = selecionaFilme.executeQuery(sql);
				
			while(rs.next())
				{
				Filme f = new Filme();
				f.setNome(rs.getString("Nome"));
				filme.add(f);
				}
			
			}
			catch(SQLException e)
				{
				imprimeErro("Erro ao listar filmes",
				e.getMessage());
			
				}
			finally
			{
			fechar();
			}
		
		
		return filme;
	}
	
	public ArrayList <Cinema> listaTodos(){
		conectar();
		ArrayList <Cinema> cinema= new ArrayList<Cinema>();
		
		try
			{
			PreparedStatement selecionaCinema = null;
			ResultSet rs = null;
			
			String sql = "SELECT * FROM CINEMA";
			
			selecionaCinema = con.prepareStatement(sql);
			rs = selecionaCinema.executeQuery(sql);
				
			while(rs.next())
				{
				Cinema c = new Cinema();
				c.setNome(rs.getString("Nome"));
				cinema.add(c);
				}
			
			}
			catch(SQLException e)
				{
				imprimeErro("Erro ao listar cinemas",
				e.getMessage());
			
				}
			finally
			{
			fechar();
			}
		
		
		return cinema;
	}
	
	public boolean validaLogin(String usuario, String senha)
	{
        String user = usuario;
        String pass = senha;
	   try
	   {
	      conectar();
	       Statement stmt = con.createStatement();
	       String CHECK_USER = "SELECT * FROM cadastro WHERE usuario = '"+ usuario +"' AND senha = '"+senha+"'";
	       ResultSet rs = stmt.executeQuery(CHECK_USER);

	        while(rs.next())
	        {
	        	System.out.println("entrou no while");
	            if(user.equals(rs.getString("usuario")))
	            {
	                if(pass.equals(rs.getString("senha")))
	                {
	                    TelaInicialGUI o = new TelaInicialGUI(usuario, senha);
	                    o.setVisible(true);
	                    return true;
	                }
	                else{
	                	JOptionPane.showMessageDialog(null, "Usuario errado");
	                	return false;
	                }
	            }
	            else {
	            	JOptionPane.showMessageDialog(null, "Usuario errado");
	            	return false;
	            }
	        }
	   }
	   catch(Exception er)
	   {
	       JOptionPane.showMessageDialog(null, "Exception:\n" + er.toString());
	   }
	return false;
	}
	
	public void AlteraFilme (String name, int qtd)
	{
	   try
	   {
		   String nome=name;
	      conectar();
	      System.out.println(nome);
	       Statement stmt = con.createStatement();
	       String CHECK_USER = "SELECT * FROM filme WHERE nome= '"+ nome +"'";
	       //Conecta no banco, prepara o banco e faz o comando sql
	       ResultSet rs = stmt.executeQuery(CHECK_USER);
	       //executa o comando sql
	       System.out.println("entrou no try");

	        while(rs.next())
	        {
	        	System.out.println("Entrou no while");
	            if(nome.equals(rs.getString("nome")))
	            {
	            	System.out.println("entrou no IF");
	                int quantidade = rs.getInt("quantidade");
	                int nvalor = quantidade-qtd;
	                if(nvalor<0){
	                	JOptionPane.showMessageDialog(null, "Quantidade de ingressos n�o dispon�vel");
	                }else{
	                	PreparedStatement alterarFilme = null;                
	                	String sql = "UPDATE filme SET quantidade =? WHERE nome = '"+ nome +"'";
	                	alterarFilme=con.prepareStatement(sql);
	                	alterarFilme.setInt(1, nvalor);
	                	
	                	int r = alterarFilme.executeUpdate();
	                	if(r > 0){
	            			//comando.executeUpdate(sql);
	            			System.out.println("Alterado!");
	        				JOptionPane.showMessageDialog(null, "Compra efetuada com sucesso");
	            		}
	                }
	            }
	            else JOptionPane.showMessageDialog(null, "Filme n�o dispon�vel");
	        }
	     fechar();
	   }
	   
	   catch(Exception er)
	   {
	       JOptionPane.showMessageDialog(null, "Exception:\n" + er.toString());
	   }
	}
	
	public void insere(Usuario u){
			conectar();
			PreparedStatement insertUsuario = null;

			try {
		
				String sql = "INSERT INTO cadastro (nome,usuario,email, senha, cidade) VALUES(?,?,?,?,?)";
				insertUsuario = con.prepareStatement(sql);
				insertUsuario.setString(1, u.getNome());
				insertUsuario.setString(2, u.getUsuario());
				insertUsuario.setString(3, u.getEmail());
				insertUsuario.setString(4, u.getSenha());
				insertUsuario.setString(5, u.getCidade());

				int r=insertUsuario.executeUpdate();

				if(r > 0){
//					comando.executeUpdate(sql);
					System.out.println("Inserida!");
					JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
					
					validaLogin(u.Usuario , u.senha);
				}
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Nome de usu�rio j� existente. Tente outro");
			} finally {
				fechar();
			}
		}
	   
	public void insereFilme(Filme f){
			conectar();
			PreparedStatement insertFilme = null;

			try {
		
				String sql = "INSERT INTO filme (nome,preco,etaria,sinopse,classificacao, imagem, quantidade) VALUES(?,?,?,?,?,?,?)";
				insertFilme = con.prepareStatement(sql);
				insertFilme.setString(1, f.getNome());
				insertFilme.setFloat(2, f.getPreco());
				insertFilme.setInt(3, f.getEtaria());
				insertFilme.setString(4, f.getSinopse());
				insertFilme.setString(5, f.getClassificacao());
				insertFilme.setString(6, f.getFoto());
				insertFilme.setInt(7, f.getQtd());
				
				int r=insertFilme.executeUpdate();

				if(r > 0){
					//comando.executeUpdate(sql);
					System.out.println("Inserida!");
				}
			} catch (SQLException e) {
				imprimeErro("Erro ao inserir Usuario", e.getMessage());
			} finally {
				fechar();
			}
		}
	      
	   public void insereCinema(Cinema c){
			conectar();
			PreparedStatement insertCinema = null;

			try {
		
				String sql = "INSERT INTO cinema (nome,endereco, CEP, imagem) VALUES(?,?,?,?)";
				insertCinema = con.prepareStatement(sql);
				insertCinema.setString(1, c.getNome());
				insertCinema.setString(2, c.getEndereco());
				insertCinema.setInt(3, c.getCEP());
				insertCinema.setString(4, c.getImagem());
				int r=insertCinema.executeUpdate();

				if(r > 0){
					//comando.executeUpdate(sql);
					System.out.println("Inserida!");
				}
			} catch (SQLException e) {
				imprimeErro("Erro ao inserir Cinema", e.getMessage());
			} finally {
				fechar();
			}
	   }
	   
	}
