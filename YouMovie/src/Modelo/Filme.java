package Modelo;

public class Filme {

	public int ID_Filme;
	public int qtd;
	public String foto;
	public String nome;
	public String sinopse;
	public int etaria;
	public String classificacao;
	public float preco;
	
	
	
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public int getQtd() {
		return qtd;
	}
	public void setQtd(int qtd) {
		this.qtd = qtd;
	}
	
	public float getPreco() {
		return preco;
	}

	public void setPreco(float preco) {
		this.preco = preco;
	}
	public int getID_Filme() {
		return ID_Filme;
	}
	public void setID_Filme(int iD_Filme) {
		ID_Filme = iD_Filme;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSinopse() {
		return sinopse;
	}
	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}
	public int getEtaria() {
		return etaria;
	}
	public void setEtaria(int etaria) {
		this.etaria = etaria;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	
}
