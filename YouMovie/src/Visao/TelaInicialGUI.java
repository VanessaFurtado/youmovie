package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class TelaInicialGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaInicialGUI frame = new TelaInicialGUI("", "");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaInicialGUI(String login, String senha) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(TelaInicialGUI.class.getResource("/Imagens/buscar.jpg")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			BuscaPorFilmeGUI b = new BuscaPorFilmeGUI(login, senha);
			b.setVisible(true);
			}
		});
		
		JButton btnCadastrarFilme = new JButton("Cadastrar Filme");
		btnCadastrarFilme.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		btnCadastrarFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			CadastrarFilmeGUI c = new CadastrarFilmeGUI();
			c.setVisible(true);
				
			}
		});
		
		JButton btnCadastrarCinema = new JButton("Cadastrar Cinema");
		btnCadastrarCinema.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		
				CadastrarCinemaGUI c = new CadastrarCinemaGUI();
				c.setVisible(true);
			}
		});
		btnCadastrarCinema.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		btnCadastrarCinema.setBounds(1078, 539, 211, 23);
		contentPane.add(btnCadastrarCinema);
		btnCadastrarFilme.setBounds(10, 507, 170, 42);
		contentPane.add(btnCadastrarFilme);
		btnNewButton.setBounds(175, 285, 289, 97);
		contentPane.add(btnNewButton);
		
		JButton btnVerCinemasNa = new JButton("");
		btnVerCinemasNa.setIcon(new ImageIcon(TelaInicialGUI.class.getResource("/Imagens/cinema.jpg")));
		btnVerCinemasNa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			BuscaPorCinema c = new BuscaPorCinema(login, senha);
			c.setVisible(true);
			}
		});
		btnVerCinemasNa.setBounds(867, 285, 289, 97);
		contentPane.add(btnVerCinemasNa);
		
		JLabel fundo = new JLabel("");
		fundo.setIcon(new ImageIcon(LoginGUI.class.getResource("/imagens/teste2.jpg")));
		fundo.setBounds(0, -18, 3000, 735);
		contentPane.add(fundo);
	}
}
