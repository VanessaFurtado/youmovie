package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoYouMovie;
import Modelo.Cinema;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class CadastrarCinemaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEndereco;
	private JTextField txtCEP;
	public String mapa;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarCinemaGUI frame = new CadastrarCinemaGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarCinemaGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnVoltar = new JButton("");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		btnVoltar.setIcon(new ImageIcon(CadastrarCinemaGUI.class.getResource("/Imagens/voltar.jpg")));
		btnVoltar.setBounds(10, 27, 47, 45);
		contentPane.add(btnVoltar);
		
		JLabel lblNomeDoCinema = new JLabel("Nome do Cinema");
		lblNomeDoCinema.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblNomeDoCinema.setBounds(10, 119, 322, 39);
		contentPane.add(lblNomeDoCinema);
		
		txtNome = new JTextField();
		txtNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtNome.setBounds(10, 169, 304, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o");
		lblEndereco.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblEndereco.setBounds(10, 305, 191, 20);
		contentPane.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtEndereco.setBounds(10, 336, 459, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JButton btnCadastrarCinema = new JButton("Cadastrar Cinema");
		btnCadastrarCinema.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		btnCadastrarCinema.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			String nome = txtNome.getText();
			String endereco = txtEndereco.getText();
			String CEP = txtCEP.getText();

			
			Cinema c = new Cinema();
			c.setNome(nome);
			c.setEndereco(endereco);
			c.setImagem(mapa);
			c.setCEP(Integer.parseInt(CEP));
			
			DaoYouMovie d = new DaoYouMovie();
			d.insereCinema(c);
			
			JOptionPane.showMessageDialog(null, "Cadastrado");
			
			}
		});
		btnCadastrarCinema.setBounds(583, 481, 191, 39);
		contentPane.add(btnCadastrarCinema);
		
		JLabel lblCep = new JLabel("CEP");
		lblCep.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblCep.setBounds(711, 131, 87, 20);
		contentPane.add(lblCep);
		
		txtCEP = new JTextField();
		txtCEP.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtCEP.setBounds(711, 172, 176, 20);
		contentPane.add(txtCEP);
		txtCEP.setColumns(10);
		
		JButton btnInserirMapa = new JButton("Inserir Mapa");
		btnInserirMapa.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		btnInserirMapa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showDialog(CadastrarCinemaGUI.this, "Attach");
				if (e.getSource() == btnInserirMapa) {

			        if (returnVal == JFileChooser.APPROVE_OPTION) {
			            File file = fc.getSelectedFile();
			            mapa=file.getAbsolutePath();
			        } else {

			        }
				}
			}
		});
		btnInserirMapa.setBounds(686, 288, 150, 39);
		contentPane.add(btnInserirMapa);
		
		JLabel fundo = new JLabel("");
		fundo.setIcon(new ImageIcon(LoginGUI.class.getResource("/imagens/teste2.jpg")));
		fundo.setBounds(0, -18, 3000, 735);
		contentPane.add(fundo);
	}
}
