package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoYouMovie;
import Modelo.Cinema;
import Modelo.Filme;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class BuscaPorCinema extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEndereco;
	private JLabel lblMapa;
	private JTextField txtCEP;
	private JButton button;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscaPorCinema frame = new BuscaPorCinema("", "");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscaPorCinema(String usuario, String senha) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoCinema = new JLabel("Nome do Cinema");
		lblNomeDoCinema.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblNomeDoCinema.setBounds(10, 224, 302, 26);
		contentPane.add(lblNomeDoCinema);
		
		txtNome = new JTextField();
		txtNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtNome.setBounds(10, 261, 324, 26);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JButton btnBuscar = new JButton("");
		btnBuscar.setIcon(new ImageIcon(BuscaPorCinema.class.getResource("/Imagens/buscarCinema.jpg")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String nome = txtNome.getText();
			System.out.println(nome);
			DaoYouMovie d = new DaoYouMovie();
			ArrayList <Cinema> consultaCinema = new ArrayList<Cinema>();
			
			try {
				consultaCinema = d.consultaCinema(nome);
				
				if(consultaCinema.isEmpty()){
						JOptionPane.showMessageDialog(null, "Cinema n�o cadastrado");
				}else{
				for(int a=0; a<consultaCinema.size(); a++){
					txtEndereco.setText(consultaCinema.get(a).endereco);
					txtCEP.setText(String.valueOf(consultaCinema.get(a).CEP));
					javax.swing.ImageIcon i = new javax.swing.ImageIcon(consultaCinema.get(a).imagem);
					lblMapa.setIcon((i));
				}}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			}
			
		});
		btnBuscar.setBounds(426, 244, 116, 43);
		contentPane.add(btnBuscar);
		
		txtEndereco = new JTextField();
		txtEndereco.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtEndereco.setBounds(10, 368, 341, 26);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endereco");
		lblEndereco.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblEndereco.setBounds(10, 331, 89, 26);
		contentPane.add(lblEndereco);
		
		lblMapa = new JLabel("");
		lblMapa.setBounds(588, 178, 764, 328);
		contentPane.add(lblMapa);
		
		JLabel lblCep = new JLabel("CEP");
		lblCep.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblCep.setBounds(10, 428, 46, 24);
		contentPane.add(lblCep);
		
		txtCEP = new JTextField();
		txtCEP.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtCEP.setBounds(10, 463, 123, 20);
		contentPane.add(txtCEP);
		txtCEP.setColumns(10);
		
		button = new JButton("");
		button.setIcon(new ImageIcon(BuscaPorCinema.class.getResource("/Imagens/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			}
		});
		button.setBounds(10, 135, 49, 45);
		contentPane.add(button);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				BuscaPorFilmeGUI c = new BuscaPorFilmeGUI(usuario, senha);
				c.setVisible(true);
			}
		});
		btnNewButton.setIcon(new ImageIcon(BuscaPorCinema.class.getResource("/Imagens/ir para.jpg")));
		btnNewButton.setBounds(944, 517, 397, 43);
		contentPane.add(btnNewButton);
		
		JLabel fundo = new JLabel("");
		fundo.setIcon(new ImageIcon(LoginGUI.class.getResource("/imagens/teste2.jpg")));
		fundo.setBounds(0, -18, 3000, 735);
		contentPane.add(fundo);
	}
}
