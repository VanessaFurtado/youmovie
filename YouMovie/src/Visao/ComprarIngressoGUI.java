package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoYouMovie;
import Modelo.Cinema;
import Modelo.Filme;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.Font;

public class ComprarIngressoGUI extends JFrame {

	private JPanel contentPane;
	private static JTextField txtNome;
	private JTextField txtClassificacao;
	private JTextField txtPreco;
	private JTextField txtCartao;
	private JTextField txtCodigoSeguranca;
	private JTextField txtValidade;
	private JTextField txtNomeCarto;
	private JTextField txtPrecoFinal;
	private JTextField txtEndereco;
	private static String nome;
	private static int etaria;
	private static int preco;
	private static String classificacao;
	private static String sinopse;
	private JTextField txtNomeCliente;
	private String nomeCliente;
	private JComboBox<String> comboBox;
	private float precof;
	private int qtd;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComprarIngressoGUI frame = new ComprarIngressoGUI("",0,0,"","", "");
					frame.setVisible(true);
					txtNome.setText(nome);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ComprarIngressoGUI(String nomeFilme, float preco, int etaria, String Classificacao, String Sinopse, String nomeCliente) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoFilme = new JLabel("Nome do Filme");
		lblNomeDoFilme.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblNomeDoFilme.setBounds(32, 206, 122, 20);
		contentPane.add(lblNomeDoFilme);
		
		txtNome = new JTextField();
		txtNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtNome.setBounds(10, 231, 197, 31);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		txtNome.setText(nomeFilme);
		
		JLabel lblQuantidade = new JLabel("Quantidade");
		lblQuantidade.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblQuantidade.setBounds(552, 267, 94, 31);
		contentPane.add(lblQuantidade);
		
		JComboBox cmbQuantidade = new JComboBox();
		cmbQuantidade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				qtd = Integer.parseInt((String) cmbQuantidade.getSelectedItem());
				precof = Float.parseFloat(txtPreco.getText())* qtd;
				txtPrecoFinal.setText(String.valueOf(precof));
			}
		});
		cmbQuantidade.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
		cmbQuantidade.setBounds(578, 303, 53, 31);
		contentPane.add(cmbQuantidade);
		
		JLabel lblClassificao = new JLabel("Classifica\u00E7\u00E3o");
		lblClassificao.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblClassificao.setBounds(283, 206, 105, 20);
		contentPane.add(lblClassificao);
		
		txtClassificacao = new JTextField();
		txtClassificacao.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtClassificacao.setBounds(294, 231, 94, 31);
		contentPane.add(txtClassificacao);
		txtClassificacao.setColumns(10);
		txtClassificacao.setText(Classificacao);
		
		JLabel lblSinopse = new JLabel("Sinopse");
		lblSinopse.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblSinopse.setBounds(1047, 136, 94, 31);
		contentPane.add(lblSinopse);
		
		JTextArea txtSinopse = new JTextArea();
		txtSinopse.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtSinopse.setBounds(801, 178, 561, 238);
		contentPane.add(txtSinopse);
		txtSinopse.setText(Sinopse);
		txtSinopse.setLineWrap(true);
		
		JLabel lblPreo = new JLabel("Pre\u00E7o");
		lblPreo.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblPreo.setBounds(517, 206, 46, 14);
		contentPane.add(lblPreo);
		
		txtPreco = new JTextField();
		txtPreco.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtPreco.setBounds(517, 231, 86, 31);
		contentPane.add(txtPreco);
		txtPreco.setColumns(10);
		txtPreco.setText(String.valueOf(preco));
		
		JLabel lblNumeroDoCarto = new JLabel("Numero do Cart\u00E3o");
		lblNumeroDoCarto.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblNumeroDoCarto.setBounds(315, 374, 144, 20);
		contentPane.add(lblNumeroDoCarto);
		
		txtCartao = new JTextField();
		txtCartao.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtCartao.setBounds(314, 394, 317, 31);
		contentPane.add(txtCartao);
		txtCartao.setColumns(10);
		
		DaoYouMovie d = new DaoYouMovie();
		String nome = d.consultaPorID(nomeCliente);
		
		JLabel lblCdigoDeSegurana = new JLabel("C\u00F3d. de Seguran\u00E7a");
		lblCdigoDeSegurana.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblCdigoDeSegurana.setBounds(398, 479, 141, 20);
		contentPane.add(lblCdigoDeSegurana);
		
		txtCodigoSeguranca = new JTextField();
		txtCodigoSeguranca.setBounds(421, 510, 89, 31);
		contentPane.add(txtCodigoSeguranca);
		txtCodigoSeguranca.setColumns(10);
		
		JLabel lblValidade = new JLabel("Validade");
		lblValidade.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblValidade.setBounds(586, 474, 86, 31);
		contentPane.add(lblValidade);
		
		txtValidade = new JTextField();
		txtValidade.setBounds(578, 510, 86, 31);
		contentPane.add(txtValidade);
		txtValidade.setColumns(10);
		
		JLabel lblNomeDoTitular = new JLabel("Nome do Titular do cart\u00E3o");
		lblNomeDoTitular.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblNomeDoTitular.setBounds(10, 474, 278, 31);
		contentPane.add(lblNomeDoTitular);
		
		txtNomeCarto = new JTextField();
		txtNomeCarto.setBounds(10, 510, 358, 31);
		contentPane.add(txtNomeCarto);
		txtNomeCarto.setColumns(10);
		
		JLabel lblPreoTotal = new JLabel("Pre\u00E7o Total");
		lblPreoTotal.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblPreoTotal.setBounds(1105, 482, 86, 14);
		contentPane.add(lblPreoTotal);
		
		
		txtPrecoFinal = new JTextField();
		txtPrecoFinal.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtPrecoFinal.setBounds(1230, 474, 122, 31);
		contentPane.add(txtPrecoFinal);
		txtPrecoFinal.setColumns(10);
		
		
		JButton btnComprar = new JButton("");
		btnComprar.setIcon(new ImageIcon(ComprarIngressoGUI.class.getResource("/Imagens/comprar.jpg")));
		btnComprar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				String nome = txtNome.getText();
				DaoYouMovie m = new DaoYouMovie();
				m.AlteraFilme(nome, qtd);
						
			}
		});
		btnComprar.setBounds(1230, 521, 122, 45);
		contentPane.add(btnComprar);
		
		JLabel lblCinema = new JLabel("Cinema");
		lblCinema.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblCinema.setBounds(10, 277, 75, 20);
		contentPane.add(lblCinema);
		
		JLabel lblEndereo = new JLabel("Endere\u00E7o");
		lblEndereo.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblEndereo.setBounds(252, 277, 94, 20);
		contentPane.add(lblEndereo);
		
		txtEndereco = new JTextField();
		txtEndereco.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtEndereco.setBounds(252, 308, 290, 26);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(ComprarIngressoGUI.class.getResource("/Imagens/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		button.setBounds(10, 136, 49, 45);
		contentPane.add(button);
		
		JLabel lblNomeDoCliente = new JLabel("Nome do Cliente");
		lblNomeDoCliente.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblNomeDoCliente.setBounds(10, 374, 144, 20);
		contentPane.add(lblNomeDoCliente);
		
		txtNomeCliente = new JTextField();
		txtNomeCliente.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtNomeCliente.setBounds(10, 394, 259, 31);
		contentPane.add(txtNomeCliente);
		txtNomeCliente.setColumns(10);
		txtNomeCliente.setText(nome);
		
		comboBox = new JComboBox();
		comboBox.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		DaoYouMovie y = new DaoYouMovie();
		ArrayList <Cinema> todos = y.listaTodos();
		for(Cinema c: todos){
			comboBox.addItem(c.nome);
			}
		comboBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
				String escolha = (String) comboBox.getSelectedItem();
				System.out.println(escolha);
		}});
		comboBox.setBounds(10, 308, 208, 26);
		contentPane.add(comboBox);
		
		JLabel fundo = new JLabel("");
		fundo.setIcon(new ImageIcon(LoginGUI.class.getResource("/imagens/teste2.jpg")));
		fundo.setBounds(0, -15, 3000, 735);
		contentPane.add(fundo);
	}
	
	public static void setInfo(String n, String c, int e, int p, String s ){
		nome=n;
		classificacao=c;
		etaria=e;
		preco=p;
		sinopse=s;
		System.out.println(nome);
	}
}
