package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoYouMovie;
import Modelo.Usuario;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JPasswordField;

public class LoginGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEmail;
	private JTextField txtLogin;
	private JTextField txtUsuario;
	private JTextField txtCidade;
	public static String user;
	public static String pass;
	private JPasswordField txtSenha;
	private JPasswordField txtSenhaL;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI frame = new LoginGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrese = new JLabel("Cadastre-se");
		lblCadastrese.setFont(new Font("Century Schoolbook", Font.BOLD | Font.ITALIC, 23));
		lblCadastrese.setBounds(33, 84, 211, 47);
		contentPane.add(lblCadastrese);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblNome.setBounds(33, 156, 74, 20);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtNome.setBounds(33, 179, 188, 28);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblEmail.setBounds(33, 286, 74, 20);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtEmail.setBounds(33, 309, 188, 28);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblSenha.setBounds(33, 360, 74, 22);
		contentPane.add(lblSenha);
		
		JButton btnCadastrar = new JButton("");
		btnCadastrar.setIcon(new ImageIcon(LoginGUI.class.getResource("/Imagens/cadastrar.jpg")));
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nome = txtNome.getText();
				String usuario = txtUsuario.getText();
				String email = txtEmail.getText();
				String senha = txtSenha.getText();
				String cidade = txtCidade.getText();
								
				
				Usuario u = new Usuario();
				u.setNome(nome);
				u.setUsuario(usuario);
				u.setEmail(email);
				u.setSenha(senha);
				u.setCidade(cidade);
				
				DaoYouMovie d= new DaoYouMovie();
				d.insere(u);			
			}
		});
		btnCadastrar.setBounds(33, 519, 188, 59);
		contentPane.add(btnCadastrar);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Century Schoolbook", Font.BOLD | Font.ITALIC, 23));
		lblLogin.setBounds(1132, 165, 84, 28);
		contentPane.add(lblLogin);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblUsuario.setBounds(1138, 258, 96, 22);
		contentPane.add(lblUsuario);
		
		txtLogin = new JTextField();
		txtLogin.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtLogin.setBounds(1117, 289, 141, 28);
		contentPane.add(txtLogin);
		txtLogin.setColumns(10);
		
		JLabel lblSenha_1 = new JLabel("Senha");
		lblSenha_1.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblSenha_1.setBounds(1138, 360, 78, 22);
		contentPane.add(lblSenha_1);
		
		JButton btnLogin = new JButton("");
		btnLogin.setIcon(new ImageIcon(LoginGUI.class.getResource("/Imagens/login.jpg")));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String usuario = txtLogin.getText();
			String senha = txtSenhaL.getText();
			
			DaoYouMovie d = new DaoYouMovie();
			boolean valor = d.validaLogin(usuario, senha);
			if(valor==false){
				JOptionPane.showMessageDialog(null, "Usu�rio ou senha incorretos");
			}
			user = txtLogin.getText();
			pass = txtSenhaL.getText();
			
			}
		});
		btnLogin.setBounds(1117, 473, 150, 59);
		contentPane.add(btnLogin);
		
		JLabel lblUusario = new JLabel("Usu\u00E1rio");
		lblUusario.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblUusario.setBounds(33, 218, 74, 22);
		contentPane.add(lblUusario);
		
		txtUsuario = new JTextField();
		txtUsuario.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtUsuario.setBounds(33, 243, 190, 28);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblCidade.setBounds(33, 424, 74, 22);
		contentPane.add(lblCidade);
		
		txtCidade = new JTextField();
		txtCidade.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtCidade.setBounds(33, 449, 188, 28);
		contentPane.add(txtCidade);
		txtCidade.setColumns(10);
		
		txtSenha = new JPasswordField();
		txtSenha.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtSenha.setBounds(33, 382, 188, 31);
		contentPane.add(txtSenha);
		
		txtSenhaL = new JPasswordField();
		txtSenhaL.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtSenhaL.setBounds(1117, 393, 141, 28);
		contentPane.add(txtSenhaL);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(LoginGUI.class.getResource("/Imagens/teste.jpg")));
		lblNewLabel.setBounds(0, 0, 3000, 735);
		contentPane.add(lblNewLabel);
	}
public static void setUser(String u, String p){
	user=u;
	pass=p;
}
}