package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoYouMovie;
import Modelo.Filme;
import Modelo.Usuario;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.Font;

public class BuscaPorFilmeGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtPreco;
	private JTextField txtClass;
	private JTextField txtEtaria;
	public ImageIcon image;
	public Image background;
	public Image foto;
	private JLabel lblFoto;
	private String user;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscaPorFilmeGUI frame = new BuscaPorFilmeGUI("", "");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscaPorFilmeGUI(String usuario, String senha) {
		user=usuario;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNomeDoFilme = new JLabel("Nome do Filme");
		lblNomeDoFilme.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblNomeDoFilme.setBounds(172, 133, 129, 20);
		contentPane.add(lblNomeDoFilme);
		
		txtNome = new JTextField();
		txtNome.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtNome.setBounds(81, 159, 325, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JTextArea txtSinopse = new JTextArea();
		txtSinopse.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtSinopse.setBounds(20, 304, 545, 237);
		contentPane.add(txtSinopse);
		txtSinopse.setLineWrap(true);
		
		JButton btnBuscar = new JButton("");
		btnBuscar.setIcon(new ImageIcon(BuscaPorFilmeGUI.class.getResource("/Imagens/buscarfilme.jpg")));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String nome = txtNome.getText();

			DaoYouMovie d = new DaoYouMovie();
			
			ArrayList <Filme> consultaFilme = new ArrayList<Filme>();
			
			try {
				consultaFilme = d.consultaPorNome(nome);
				System.out.println(consultaFilme.size());

				if(consultaFilme.isEmpty()){
					JOptionPane.showMessageDialog(null, "Filme n�o cadastrado");
			}else{
					for(int a=0; a<consultaFilme.size(); a++)
					{				
					txtClass.setText(consultaFilme.get(a).classificacao);
					System.out.println(consultaFilme.get(a).classificacao);
					txtEtaria.setText(String.valueOf(consultaFilme.get(a).etaria));
					System.out.println(consultaFilme.get(a).etaria);
					txtPreco.setText(String.valueOf(consultaFilme.get(a).preco));
					System.out.println(consultaFilme.get(a).sinopse);
					txtSinopse.setText(consultaFilme.get(a).sinopse);
					javax.swing.ImageIcon i = new javax.swing.ImageIcon(consultaFilme.get(a).foto);
					lblFoto.setIcon((i));
					}
			}
				}
			catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			}
		});
		btnBuscar.setBounds(426, 133, 120, 48);
		contentPane.add(btnBuscar);
		
		lblFoto = new JLabel("");
		lblFoto.setBounds(575, 133, 787, 408);
		contentPane.add(lblFoto);
		lblFoto.setOpaque(false);
		
		JButton btnComprar = new JButton("");
		btnComprar.setIcon(new ImageIcon(BuscaPorFilmeGUI.class.getResource("/Imagens/comprar.jpg")));
		btnComprar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int etaria = Integer.parseInt(txtEtaria.getText());
			float preco = Float.parseFloat(txtPreco.getText());
			DaoYouMovie d = new DaoYouMovie();
			ComprarIngressoGUI c = new ComprarIngressoGUI(txtNome.getText(), preco, etaria, txtClass.getText(), txtSinopse.getText(), user);
			c.setVisible(true);
			
			}
		});
		btnComprar.setBounds(1212, 540, 110, 29);
		contentPane.add(btnComprar);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(BuscaPorFilmeGUI.class.getResource("/Imagens/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		button.setBounds(10, 134, 49, 45);
		contentPane.add(button);
		
		txtPreco = new JTextField();
		txtPreco.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtPreco.setBounds(20, 238, 100, 20);
		contentPane.add(txtPreco);
		txtPreco.setColumns(10);
		
		txtClass = new JTextField();
		txtClass.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtClass.setBounds(185, 238, 86, 20);
		contentPane.add(txtClass);
		txtClass.setColumns(10);
		
		txtEtaria = new JTextField();
		txtEtaria.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		txtEtaria.setBounds(320, 238, 86, 20);
		contentPane.add(txtEtaria);
		txtEtaria.setColumns(10);
		
		JLabel lblPreoDoIngresso = new JLabel("Pre\u00E7o do ingresso");
		lblPreoDoIngresso.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblPreoDoIngresso.setBounds(10, 207, 138, 20);
		contentPane.add(lblPreoDoIngresso);
		
		JLabel lblClassificao = new JLabel("Classifica\u00E7\u00E3o");
		lblClassificao.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblClassificao.setBounds(172, 207, 110, 20);
		contentPane.add(lblClassificao);
		
		JLabel lblFaixaEtria = new JLabel("Faixa Et\u00E1ria");
		lblFaixaEtria.setFont(new Font("Century Schoolbook", Font.PLAIN, 16));
		lblFaixaEtria.setBounds(320, 208, 100, 19);
		contentPane.add(lblFaixaEtria);
		
		JLabel fundo = new JLabel("");
		fundo.setIcon(new ImageIcon(LoginGUI.class.getResource("/imagens/teste2.jpg")));
		fundo.setBounds(0, -18, 3000, 735);
		contentPane.add(fundo);
		

	}
}
