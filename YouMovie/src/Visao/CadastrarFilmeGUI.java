package Visao;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Dao.DaoYouMovie;
import Modelo.Filme;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class CadastrarFilmeGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtClassificacao;
	private JTextField txtPreco;
	public String foto;
	private JTextField txtEtaria;
	private JTextField txtQtd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastrarFilmeGUI frame = new CadastrarFilmeGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastrarFilmeGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-6, 0, 3000, 735);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtQtd = new JTextField();
		txtQtd.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		txtQtd.setBounds(1207, 177, 128, 30);
		contentPane.add(txtQtd);
		txtQtd.setColumns(10);
		
		JLabel lblQuantidadeDeIngressos = new JLabel("Qtd ingressos");
		lblQuantidadeDeIngressos.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblQuantidadeDeIngressos.setBounds(1214, 143, 148, 21);
		contentPane.add(lblQuantidadeDeIngressos);
		
		JLabel lblNomeDoFilme = new JLabel("Nome do Filme");
		lblNomeDoFilme.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblNomeDoFilme.setBounds(10, 143, 128, 23);
		contentPane.add(lblNomeDoFilme);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 177, 521, 30);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblClassificao = new JLabel("Classifica\u00E7\u00E3o");
		lblClassificao.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblClassificao.setBounds(1022, 143, 147, 23);
		contentPane.add(lblClassificao);
		
		txtClassificacao = new JTextField();
		txtClassificacao.setBounds(1022, 177, 175, 30);
		contentPane.add(txtClassificacao);
		txtClassificacao.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Sinopse");
		lblNewLabel.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblNewLabel.setBounds(10, 228, 105, 23);
		contentPane.add(lblNewLabel);
		
		JTextArea txtSinopse = new JTextArea();
		txtSinopse.setBounds(10, 262, 1035, 191);
		contentPane.add(txtSinopse);
		txtSinopse.setLineWrap(true);
		
		JLabel lblPreo = new JLabel("Pre\u00E7o");
		lblPreo.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblPreo.setBounds(798, 143, 55, 23);
		contentPane.add(lblPreo);
		
		txtPreco = new JTextField();
		txtPreco.setBounds(798, 177, 175, 30);
		contentPane.add(txtPreco);
		txtPreco.setColumns(10);
		
		JLabel lblFotosadicionePelo = new JLabel("Foto");
		lblFotosadicionePelo.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblFotosadicionePelo.setBounds(10, 472, 48, 23);
		contentPane.add(lblFotosadicionePelo);
		
		JButton btnCadastrarFilme = new JButton("Cadastrar Filme");
		btnCadastrarFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String nome = txtNome.getText();
			String Class = txtClassificacao.getText();
			float preco = Float.parseFloat(txtPreco.getText());
			String sinopse = txtSinopse.getText();
			int etaria = Integer.parseInt(txtEtaria.getText());
			int qtd = Integer.parseInt(txtQtd.getText());
			
			Filme f = new Filme();
			f.setNome(nome);
			f.setClassificacao(Class);
			f.setPreco(preco);
			f.setSinopse(sinopse);
			f.setEtaria(etaria);
			f.setFoto(foto);
			f.setQtd(qtd);
			
			DaoYouMovie d = new DaoYouMovie();
			d.insereFilme(f);
			
			JOptionPane.showMessageDialog(null, "Gravado");	
			}
		});
		btnCadastrarFilme.setBounds(1055, 433, 279, 113);
		contentPane.add(btnCadastrarFilme);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(CadastrarFilmeGUI.class.getResource("/Imagens/voltar.jpg")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			dispose();
			}
		});
		button.setBounds(10, 11, 49, 45);
		contentPane.add(button);
		
		JLabel lblFaixaEtria = new JLabel("Faixa Et\u00E1ria");
		lblFaixaEtria.setFont(new Font("Century Schoolbook", Font.PLAIN, 18));
		lblFaixaEtria.setBounds(587, 143, 137, 23);
		contentPane.add(lblFaixaEtria);
		
		txtEtaria = new JTextField();
		txtEtaria.setBounds(587, 177, 137, 30);
		contentPane.add(txtEtaria);
		txtEtaria.setColumns(10);

		JButton btnAdicionarFoto = new JButton("Adicionar foto");
		btnAdicionarFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(getComponent(0));
//				int returnVal = fc.showDialog(FileChooserDemo2.this, "Attach");
				if (e.getSource() == btnAdicionarFoto) {
//			        returnVal = fc.showOpenDialog(FileChooserDemo.this);

			        if (returnVal == JFileChooser.APPROVE_OPTION) {
			            File file = fc.getSelectedFile();
			            foto=file.getAbsolutePath();
			        } else {

			        }
				}
			}
		});
		btnAdicionarFoto.setBounds(104, 472, 159, 30);
		contentPane.add(btnAdicionarFoto);
		
		JLabel fundo = new JLabel("");
		fundo.setIcon(new ImageIcon(LoginGUI.class.getResource("/imagens/teste2.jpg")));
		fundo.setBounds(0, -18, 3000, 735);
		contentPane.add(fundo);
	}
}
